<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

use Doctrine\Persistence\ManagerRegistry;
use Symfony\Component\HttpFoundation\Request;
use App\Entity\Category;
#[Route('/api', name: 'api_')]
class CategoryController extends AbstractController
{
    #[Route('/category', name: 'app_category', methods: ['GET'])]
    public function index(ManagerRegistry $doctrine): JsonResponse
    {
        $categories = $doctrine
            ->getRepository(Category::class)
            ->findAll();

        $data = [];

        foreach ($categories as $category) {
            $data[] = [
                'id' => $category->getId(),
                'name' => $category->getName()
            ];
        }
        return $this->json($data);
    }

    #[Route('/category', name: 'category_create', methods: ['POST'])]
    public function createAction(ManagerRegistry $doctrine, Request $request): JsonResponse
    {
        $entityManager = $doctrine->getManager();

        $category = new Category();
        $category ->setName($request->request->get('name'));

        $entityManager->persist($category);
        $entityManager->flush();

        $data = [
            'id' => $category->getId(),
            'name' => $category->getName()
        ];

        return $this->json($data);
    }

    #[Route('/category/{id}', name: 'category_show', methods: ['GET'])]
    public function showAction(ManagerRegistry $doctrine, int $id): JsonResponse
    {
        $category = $doctrine
            ->getRepository(Category::class)
            ->find($id);

        if(!$category) {
            return $this->json('No category found for id '.$id, 404);
        }

        $data = [
            'id' => $category->getId(),
            'name' => $category->getName()
        ];

        return $this->json($data);
    }

    #[Route('/category/{id}', name: 'category_update', methods: ['PUT', 'PATCH'])]
    public function updateAction(ManagerRegistry $doctrine, Request $request, int $id): JsonResponse
    {
        $entityManager = $doctrine->getManager();
        $category = $entityManager->getRepository(Category::class)->find($id);

        if(!$category) {
            return $this->json('No category found for id '.$id, 404);
        }

        $category->setName($request->request->get('name'));
        $entityManager->flush();

        $data = [
            'id' => $category->getId(),
            'name' => $category->getName()
        ];

        return $this->json($data);
    }

    #[Route('/category/{id}', name: 'category_delete', methods: ['DELETE'])]
    public function deleteAction(ManagerRegistry $doctrine, int $id): JsonResponse
    {
        $entityManager = $doctrine->getManager();
        $category = $entityManager->getRepository(Category::class)->find($id);

        if(!$category) {
            return $this->json('No category found for id '.$id, 404);
        }

        $entityManager->remove($category);
        $entityManager->flush();

        return $this->json('Deleted a category successfully with id '.$id);
    }

}
